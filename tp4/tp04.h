#include <stdio.h>
#include <stdlib.h>

#define TST_ALL

char *miroir (const char *s);
char *saisie ();

#include "test_tp04.h"

/* Ne pas utiliser l'allocation maison */
/* 
#define my_malloc malloc
#define my_realloc realloc
*/