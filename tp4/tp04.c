#include "tp04.h"

#include <stdarg.h>
#include <ctype.h>
#include <string.h>

#define my_malloc malloc
#define my_realloc realloc
#define init_alloc(x) while(0)

char *miroir (const char *s) {
	char *miroir;
	int size = 0, i = 0;

	init_alloc();

	/* On calcule la taille de s */
	while (*(s + size) != '\0' ) {
		size++;
	}

	/* On alloue une zone mémoire de la taille de s + 1 à miroir (+1 pour le caractère d'échappement) */
	miroir = my_malloc((size + 1)*sizeof(char));
	if (miroir == NULL) return NULL; 
	size--; /* On réduit size parce qu'on commence à l'indice zéro */

	/* RAPPEL : size est maintenant égal à la taille de s */
	while (size >= 0) {
		*( miroir + i*sizeof(char) ) = *( s + size*sizeof(char) );
		size--;
		i++;
	}

	/* On lui applique le caractère d'échappement 
	   RAPPEL : Le i++ est déjà exécuté au dernier tour de boucle */
	*( miroir + i*sizeof(char) ) = '\0';

	return miroir;
}

char *saisie () {
	char character, *saisie = my_malloc(sizeof(char)), *saisie_tmp;
	int i = 0;
	
	/* Tant que le caractère trouvé est différent de l'espace */
	while ( !isspace(character) )  {
		/* Entrée utilisateur */
		character = getchar();

		/* On l'applique à la chaine de caractère */
		*(saisie + i*sizeof(char)) = character;

		/* Réallocation avec vérification */
		saisie_tmp = my_realloc(saisie, (i + 1)*sizeof(char));

		if (saisie_tmp == NULL) {
			return NULL;
		} else {
			saisie = saisie_tmp;
		}
		
		i++;
	}

	/* On lui applique le caractère d'échappement 
	   RAPPEL : Un i++ a été fait au dernier tout de boucle 
	   donc on le décrémente de 1 */
	*( saisie + (i - 1)*sizeof(char) ) = '\0';
	
	return saisie;
}

/* Erreurs */
#define ARGUMENTS_CORRECTS ( verification(*(argv + i), 'm') == 1 || verification(*(argv + i), 's') == 1 )
#define AUCUNE_OPTION ( mFlag == 0 && sFlag == 0 )
#define PAS_D_ARGUMENT ( ( argc == 1 ) )

#define AUCUN_ARGUMENT_RECEVABLE ( AUCUNE_OPTION && chaineFlag > 0 && argc > 2 ) /* Laisse passer les echos */
#define ARGUMENTS_ET_OPTIONS_ILLEGALES ( sFlag > 0 && chaineFlag > 1 ) /* -s avec un argument ne doit pas passer */

/* Différents cas */
#define ERROR_FLAG ( errorFlag > 0 )
#define M_FLAG ( mFlag > 0 && sFlag == 0 )
#define S_FLAG ( sFlag > 0 && mFlag == 0 )
#define SM_FLAG ( mFlag > 0 && sFlag > 0 )
#define ECHO ( AUCUNE_OPTION && chaineFlag > 0 )

int verification(char *chaine, char option) {
	int i = 0;
	while ( *(chaine + i) != '\0' ) {
		if ( *(chaine + i) == option ) {
			return 1;
		}
		i++;
	}

	return 0;
}

int main(int argc, char *argv[]) {
	int i, mFlag = 0, sFlag = 0, chaineFlag = 0, errorFlag = 0;
	char *chaine;

	init_alloc();

	/* On mets à jour le drapeau de chacune des options */
	for (i = 0; i < argc; i++) {
		/* Si le premier caractère de l'argument testé est un tiret */
		if ( *(argv + i)[0] == '-' ) {
			if ( ARGUMENTS_CORRECTS ) {
				mFlag += verification(*(argv + i), 'm');
				sFlag += verification(*(argv + i), 's');
			} else {
				errorFlag ++;
			}
		} else {
			chaineFlag++;
			chaine = *(argv + i);
		}
	}

	/* Si -s, aucun autre argument n'est recevable */
	if ( AUCUN_ARGUMENT_RECEVABLE || PAS_D_ARGUMENT || ARGUMENTS_ET_OPTIONS_ILLEGALES ) {
		errorFlag ++;
	}

	/* On agis selon les différents drapeaux activées */
	if ( ERROR_FLAG ) { /* En cas d'erreur */
		printf("mauvaise utilisation\n");
		return 1;
	} else if ( M_FLAG ) { /* -m */
		printf("%s\n", miroir(chaine));	
	} else if ( S_FLAG ) { /* -s */
		printf("%s\n", saisie());
	} else if ( SM_FLAG ) { /* -ms */
		printf("%s\n", miroir(saisie()));
	} else if ( ECHO ) { /* echo */
		printf("%s\n", chaine);
	}
	return 0;
}