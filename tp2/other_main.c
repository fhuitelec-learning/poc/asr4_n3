#include "tp02.h"

void questionUn(char s[]) {

	printf("\nEX1 - Question 1\n");
	printf("%d\n", char_mon_strlen(s));
}	

void questionDeux(const char s1[], const char s2[]) {
	printf("\nEX1 - Question 2\n");
	printf("s1 : %d\ns2 : %d\n", mon_strlen(s1), mon_strlen(s2));
}

void questionTrois(const char *s1, const char *s2) {
	printf("\nEX1 - Question 3\n");
	printf("Résultat : %d\n", mon_strcmp(s1, s2));
}

void questionQuatre() {
	char s1[] = "ceci", s2[] = "ceci22";
	printf("\nEX1 - Question 4\n");
	printf("Chaines utilisées pour cette question : \n\"%s\"\n\"%s\"\n", s1, s2);
	printf("Résultat avec %d : %d\n", -1, mon_strncmp(s1, s2, -1));
	printf("Résultat avec %d : %d\n", 4, mon_strncmp(s1, s2, 4));
	printf("Résultat avec %d, s2 en premier : %d\n", 5, mon_strncmp(s2, s1, 5));
	printf("Résultat avec %d, s1 en premier : %d\n", 5, mon_strncmp(s1, s2, 5));
	printf("Résultat avec %d : %d\n", 22, mon_strncmp(s1, s2, 22));
}

void questionCinq(char *s1, const char *s2) {
	printf("\nEX1 - Question 5\n");
	printf("Résultat de la concaténation : \n%s\n", mon_strcat(s1, s2));
}

void questionSix(char *s1) {
	int i = 109, j = 97, k = 101;

	printf("\nEX1 - Question 6\n");
	printf("Caractère numéro %d (m) : %s\n", i, mon_strchr(s1, i));
	printf("Caractère numéro %d (a) : %s\n", j, mon_strchr(s1, j));
	printf("Caractère numéro %d (e) : %s\n", k, mon_strchr(s1, k));
}

void questionSept() {
	char s1[] = "Le caca, c'est délicieux", s2[] = "c'est";

	printf("\nEX1 - Question 7\n");
	printf("Chaines utilisées pour cette question : \n\"%s\"\n", s1);
	printf("Résultat avec \"%s\" : \"%s\" \n", s2, mon_strstr(s1, s2));

}

int main(int argc, char *argv[]) {
	char s1[100] = "Ceci est une chaine"; /* Longueur : 19 */
	char s2[] = "Ceci est une deuxieme chaine"; /* Longueur : 28 */
	
	printf("\nChaine utilisée pour les question 1, 2 et 6 : \"%s\"\n", s1);
	printf("Chaine utilisée pour les question 3 et 5 (en plus de la première) : \"%s\"\n", s2);

	questionUn(s1);
	questionDeux(s1, s2);
	questionTrois(s1, s2);
	questionQuatre();
	questionCinq(s1, s2);
	questionSix(s1);
	questionSept();
	return 0;
}