#include <stdio.h>

/* Argument : char[] */
int char_mon_strlen(char s[]) {
	int i = 0;

	/* Tant que le caractère d'échappement n'est pas trouvé, on compte */
	while (s[i] != '\0') {
		i++;
	}

	return i;
}

int mon_strlen(const char *s) { /* Argument : pointeur */
	int i = 0;

	/* Tant que le caractère d'échappement n'est pas trouvé, on compte */
	while (*(s + i) != '\0') {
		i++;
	}

	return i;
}

int mon_strcmp(const char *s1, const char *s2) {
	/* Author : SquierDotQn@github.com, modified by fydh@github.com */
	int i = 0;
	
	/* Tant que le caractère d'échappement n'a été trouvé dans aucune des
	2 chaines */
	while( *(s1 + i) != '\0' && *(s2 + i) != '\0' ) {
		/* Tant que s1 et s2 à la case i sont égaux, i est incrémenté */
        if(*(s1 + i) == *(s2 + i)) {
                i++;
        } else if (*(s1 + i) > *(s2 + i)) {
                return 1;
        } else if (*(s1 + i) < *(s2 + i)) {
                return -1;
        }
    }

    /* Test dans le cas où les chaines sont strictement identiques jusqu'à la fin d'une des 2 chaines
    Ex : s1 : "ceci", s2 : "ceci est" -> s2 aura "l'avantage"
    Si la chaine comparée est la plus grande, la valeur renvoyée sera positive */
    if( mon_strlen(s1) != mon_strlen(s2) ) {
    	return mon_strlen(s1) - mon_strlen(s2);
    }
    return 0;
}

int mon_strncmp(const char *s1, const char *s2, int n) {
	/* Author : SquierDotQn@github.com, modified by fydh@github.com */
	int i = 0;
	
	/* Tant que le caractère d'échappement n'a été trouvé dans aucune des
	2 chaines et qu'on ne dépasse pas l'indice donné en paramètre */
	while( *(s1 + i) != '\0' && *(s2 + i) != '\0' && i < n ) {
		/* Tant que s1 et s2 à la case i sont égaux, i est incrémenté */
        if(*(s1 + i) == *(s2 + i)) {
                i++;
        } else if (*(s1 + i) > *(s2 + i)) {
                return 1;
        } else if (*(s1 + i) < *(s2 + i)) {
                return -1;
        }
    }

	/* Test dans le cas où les chaines sont strictement identiques jusqu'à la fin d'une des 2 chaines et dans la limite de n
    Ex : s1 : "ceci", s2 : "ceci est", n > mon_strlen(s1) -> s2 aura "l'avantage"
    Si la chaine comparée est la plus grande, la valeur renvoyée sera positive */
    if( (mon_strlen(s1) < n) || (mon_strlen(s2) < n) ) {
        if( mon_strlen(s1) != mon_strlen(s2) ) {
    		return mon_strlen(s1) - mon_strlen(s2);
    	}
	}
	return 0;
}

char *mon_strcat(char *s1, const char *s2) {
	int i = 0, j;

	i = mon_strlen(s1);

	/* i est initialisé à la taille de s1 pour commencer à partir de la fin de s1 */
	for (j = 0; *(s2 + j) != '\0'; i++, j++) {
		/* On rajoute le contenu de s2 à partir de la fin de s1
		jusqu'à ce que le caractère d'échappement de s2 soit atteint */
		s1[i] = s2[j];
	}

	/* On ajoute caractère d'échappement à la fin de la chaine concaténée */
	s1[i] = '\0';

	return s1;
}

char *mon_strchr(const char *s, int c) {
	int i = 0;

	/* Parcours de la chaine dans la limite de sa longueur */
	while (s[i] != '\0' ) {
		/* Si le caractère rencontré est égal à celui en paramètre */
		if (s[i] == (char) c) { /* /!\ Attention au cast /!\ */
			return (char *)&s[i]; 
			/* Accrochez-vous : 
			On renvoie le cast en pointeur de caractère : (char *)
			l'adresse de la case où on a rencontré le caractère recherché : &s[i]
			&s[i] équivaut à (s + i) 	-> Pointeur de la première case du tableau s + i
										-> (char *)(s + i)
			*/
		}
		i++;
	}
	return NULL;
}

char *mon_strstr(char *haystack, char *needle) {
	int i;

	/* On parcours toute la chaine haystack */
	for (i = 0; i < mon_strlen(haystack); i++) {
		/* À chaque i, on compare la chaine haystack[i] à la chaine needle
		sur toute la longueur de la chaine needle */
		if ( mon_strncmp((haystack + i), needle, mon_strlen(needle)) == 0 ) {
			return (haystack + i);
			/* On retourne la chaine à partir de l'indice i */
		}
	}
	return NULL;
}