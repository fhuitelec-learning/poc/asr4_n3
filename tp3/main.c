#include <stdlib.h>
#include <string.h>

#include <minunit.h>
#include "tp03.h"

/* Verification  HEAPSIZE > 2047 */
#ifndef HEAPSIZE
#error HEAPSIZE should be defined before compiling main.c
#endif

#if (HEAPSIZE < 2048)
#error HEAPSIZE should be >= 2048
#endif

#ifndef NULL
#define NULL (0)
#endif

unsigned char heap[HEAPSIZE] ;
#define SIZEOF_UINT ((sizeof(unsigned int)))
#define HEAPSIZE_INT (HEAPSIZE / SIZEOF_UINT)
#define TRUE (-1)
#define FALSE (0)
#define MAGIC (0x55)

struct int2byte {
    union {
        unsigned char c[SIZEOF_UINT] ;
        unsigned int i ;
    } u ;
} convert ;

static void mu_write_int(int offset, int value) {
    int i ;
    unsigned char *ptr = heap + offset ;
    convert.u.i = value ;
    for (i = 0 ; i < SIZEOF_UINT ; i++) {
        *ptr = convert.u.c[i] ;
        ptr++ ;
    }
}
/****************** get_int *******************/

static void mu_init_heap() {
    int i ;
    for (i = 0 ; i < HEAPSIZE ; i++) {
    heap[i] = MAGIC ;
    mu_write_int(0 * SIZEOF_UINT, 18) ;
    mu_write_int(12 * SIZEOF_UINT, 44) ;
    mu_write_int(400 * SIZEOF_UINT, 0xFEFDFCFB) ;
    mu_write_int(HEAPSIZE - SIZEOF_UINT, 1000) ;
    }
}

/****************** get_int *******************/

MU_SETUP(get_int) {
    mu_init_heap() ;
}
MU_TEAR_DOWN(get_int) {
}
MU_TEST(get_int, get_int_test_0) {
    MU_ASSERT_EQUAL(get_int(heap+SIZEOF_UINT*0), 18) ;
}
MU_TEST(get_int, get_int_test_12) {
    MU_ASSERT_EQUAL(get_int(heap+SIZEOF_UINT*12), 44) ;
}
MU_TEST(get_int, get_int_test_120) {
    MU_ASSERT_EQUAL(get_int(heap+SIZEOF_UINT*400), 0xFEFDFCFB) ;
}
MU_TEST(get_int, get_int_test_last) {
    unsigned int last = (HEAPSIZE / SIZEOF_UINT)-1 ;
    MU_ASSERT_EQUAL(get_int(heap+SIZEOF_UINT*last), 1000) ;
}
MU_TEST_SUITE(get_int) = {
    MU_ADD_TEST(get_int, get_int_test_0),
    MU_ADD_TEST(get_int, get_int_test_12),
    MU_ADD_TEST(get_int, get_int_test_120),
    MU_ADD_TEST(get_int, get_int_test_last),
    MU_TEST_SUITE_END
} ;

/****************** set_int *******************/

MU_SETUP(set_int) {
    mu_init_heap() ;
}
MU_TEAR_DOWN(set_int) {
}
static int mu_check(unsigned int addr, unsigned int val) {
    unsigned char *ptr ;
    ptr = heap + SIZEOF_UINT * addr ;
    return *((unsigned int *)ptr) == val ;
}
MU_TEST(set_int, set_int_test_0) {
    set_int((heap+0), 0) ;
    MU_ASSERT_TRUE(mu_check(0, 0)) ;
}
MU_TEST(set_int, set_int_test_all) {
    int i ;
    /* creating a pattern of SIZEOF_UINT bytes length (max 8 bytes) */
    unsigned char char_pat[8] = {0x55, 0xAA, 0xA5, 0x5A, 0x55, 0xAA, 0xA5, 0x5A } ;
    unsigned int *int_pat = (unsigned int *) char_pat ;

    unsigned int *ptr = (unsigned int *) heap ;
    for (i = 0 ; i < HEAPSIZE_INT ; i++) {
        *ptr = i ^ (*int_pat) ;
        ptr++ ;
    }
    for (i = 0 ; i < HEAPSIZE_INT ; i++) {
        MU_ASSERT_TRUE(mu_check(i, i ^ (*int_pat))) ;
    }
}
MU_TEST_SUITE(set_int) = {
    MU_ADD_TEST(set_int, set_int_test_0),
    MU_ADD_TEST(set_int, set_int_test_all),
    MU_TEST_SUITE_END
} ;

/****************** Setup and Test functions for memory chunks *******************/

typedef struct {
    int start ;     /*  start offset of chunk (included) */
    int status ;    /* free status of chunk */
    int end ;       /*end offset of chunk (excluded) */
} bloc ;

static unsigned int *addr2ptr(int addr) {
    return (unsigned int *) (heap + addr) ;
}

static void mu_set_bloc(bloc b) {
    unsigned char *ptr ;
    ptr = heap+b.start ;
    set_int(ptr, b.status) ;
    set_int(ptr+SIZEOF_UINT, b.end - b.start) ;
    ptr = heap+b.end-SIZEOF_UINT ;
    set_int(ptr, b.end-b.start) ;
}

#define CHECK_ADDR(addr, val) do { \
    int read = *addr2ptr(addr) ;    \
    MU_ASSERT_EQUAL(read, val) ;    \
    } while (0)

#define CHECK_BLOC(bloc) do {   \
    int size = (bloc.end) - (bloc.start) ;  \
    CHECK_ADDR(bloc.start, bloc.status) ;   \
    CHECK_ADDR(bloc.start + SIZEOF_UINT, size) ;  \
    CHECK_ADDR(bloc.end - SIZEOF_UINT, size) ;    \
    } while (0)

#define CHECK_CHUNK(current, expected) do { \
    MU_ASSERT_EQUAL(current.free, expected.free) ; \
    MU_ASSERT_EQUAL(current.addr, expected.addr) ; \
    MU_ASSERT_EQUAL(current.size, expected.size) ; \
    MU_ASSERT_EQUAL(current.previous_chunk, expected.previous_chunk) ; \
    MU_ASSERT_EQUAL(current.next_chunk, expected.next_chunk) ;  \
}   while (0) \

/****************** set_chunk *******************/

MU_SETUP(set_chunk) {
    mu_init_heap() ;
}
MU_TEAR_DOWN(set_chunk) {
}
MU_TEST(set_chunk, set_chunk_simple) {
    chunk c = { FREE, 300, NULL, NULL, NULL } ;
    bloc b = { 40, FREE, 340 } ;
    set_chunk(&c, heap + 40) ;
    CHECK_BLOC(b) ;
}

MU_TEST(set_chunk, set_chunk_top) {
    chunk c = { BUSY, 1000, NULL, NULL, NULL } ;
    bloc b = { 0, BUSY, 1000 } ;
    set_chunk(&c, heap + 0) ;
    CHECK_BLOC(b) ;
}

MU_TEST(set_chunk, set_chunk_bottom) {
    chunk c = { FREE, 1024, NULL, NULL, NULL } ;
    bloc b = { HEAPSIZE-1024, FREE, HEAPSIZE } ;
    set_chunk(&c, heap + HEAPSIZE - 1024) ;
    CHECK_BLOC(b) ;
}

MU_TEST(set_chunk, set_chunk_twin) {
    chunk c1 = { FREE, 512, NULL, NULL, NULL } ;
    chunk c2 = { BUSY, 100, NULL, NULL, NULL } ;
    bloc b1 = { 512, FREE, 1024 } ;
    bloc b2 = { 1200, BUSY, 1300 } ;
    set_chunk(&c1, heap + 512) ;
    set_chunk(&c2, heap + 1200) ;
    CHECK_BLOC(b1) ;
    CHECK_BLOC(b2) ;
}

MU_TEST_SUITE(set_chunk) = {
    MU_ADD_TEST(set_chunk, set_chunk_simple),
    MU_ADD_TEST(set_chunk, set_chunk_top),
    MU_ADD_TEST(set_chunk, set_chunk_bottom),
    MU_ADD_TEST(set_chunk, set_chunk_twin),
    MU_TEST_SUITE_END
} ;

/****************** get_chunk *******************/

static bloc get_chunk_init[] = {{   0, BUSY,  100},    /* c0 */
                                { 100, BUSY,  512},
                                { 512, BUSY, 1000},
                                {1000, FREE, 1400},    /* c3 */
                                {1400, BUSY, 1800},
                                {1800, BUSY, 2000},
                                {2000, BUSY, HEAPSIZE}} ;  /* c6 */

static chunk c0 = {BUSY, 100, heap     , heap+ 100, 0} ;
static chunk c3 = {FREE, 400, heap+1000, heap+1400, heap+512} ;
static chunk c6 = {BUSY,  HEAPSIZE-2000, heap+2000,         0, heap+1800} ;

MU_SETUP(get_chunk) {
    int i ;
    for (i = 0 ; i < HEAPSIZE ; i++) {
        heap[i] = MAGIC ;
    }
    for (i= 0 ; i < sizeof(get_chunk_init)/sizeof(bloc); i++) {
        mu_set_bloc(get_chunk_init[i]) ;
    }
}
MU_TEAR_DOWN(get_chunk) {
}
MU_TEST(get_chunk, top_chunk) {
    chunk c ;
    get_chunk(&c, heap+0) ;
    CHECK_CHUNK(c, c0) ;
}
MU_TEST(get_chunk, standard_chunk) {
    chunk c ;
    get_chunk(&c, heap+1000) ;
    CHECK_CHUNK(c, c3) ;
}
MU_TEST(get_chunk, bottom_chunk) {
    chunk c ;
    get_chunk(&c, heap+2000) ;
    CHECK_CHUNK(c, c6) ;
}
MU_TEST_SUITE(get_chunk) = {
    MU_ADD_TEST(get_chunk, top_chunk),
    MU_ADD_TEST(get_chunk, standard_chunk),
    MU_ADD_TEST(get_chunk, bottom_chunk),
    MU_TEST_SUITE_END
} ;

/************************************************/

/****************** init_alloc *******************/

MU_SETUP(init_alloc) {
}
MU_TEAR_DOWN(init_alloc) {
}
MU_TEST(init_alloc, init) {
    bloc b = {0, FREE, HEAPSIZE} ;
    init_alloc() ;
    CHECK_BLOC(b) ;
}
MU_TEST_SUITE(init_alloc) = {
    MU_ADD_TEST(init_alloc, init),
    MU_TEST_SUITE_END
} ;

/************************************************/

/****************** my_malloc *******************/

static bloc my_malloc_init[] = {{   0, FREE,   30},    /* c0:  30 octets libres   */
                                {  30, BUSY,   45},    /* c1:  10 octets réservés */
                                {  45, FREE,  100},    /* c2:  55 octets libres   */
                                { 100, BUSY,  150},    /* c3:  50 octets réservés */
                                { 150, BUSY,  200},    /* c4:  50 octets réservés */
                                { 200, FREE, 1000},    /* c5: 800 octets libres   */
                                {1000, BUSY, 1100},    /* c6: 100 octets réservés */
                                {1100, FREE, HEAPSIZE}} ;  /* c7: 948 octets libres   */

MU_SETUP(my_malloc) {
    int i ;
    for (i = 0 ; i < HEAPSIZE ; i++) {
        heap[i] = MAGIC ;
    }
    for (i= 0 ; i < sizeof(my_malloc_init)/sizeof(bloc); i++) {
        mu_set_bloc(my_malloc_init[i]) ;
    }
}
MU_TEAR_DOWN(my_malloc) {
}
MU_TEST(my_malloc, c0_alloc_split_selected_chunk) {
    bloc current = {0, BUSY, 2+3*SIZEOF_UINT } ;
    bloc next = { 2+3*SIZEOF_UINT, FREE, 30} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(2) ;
    MU_ASSERT_EQUAL(ptr, heap+2*SIZEOF_UINT) ;

    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c0_alloc_keep_selected_chunk) {
    bloc current = {0, BUSY, 30 } ;
    bloc next = { 30, BUSY, 45} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(10) ;
    MU_ASSERT_EQUAL(ptr, heap+2*SIZEOF_UINT) ;

    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c0_alloc_exact_fit) {
    bloc current = {0, BUSY, 30 } ;
    bloc next = { 30, BUSY, 45} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(30-6*sizeof(unsigned int)) ;
    MU_ASSERT_EQUAL(ptr, heap+2*SIZEOF_UINT) ;

    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c2_alloc_split_selected_chunk) {
    bloc prev = {30, BUSY, 45 } ;
    bloc current = {45, BUSY, 77 } ; /* 67 = 45 + 20 (requested) + 3 * SIZEOF_UINT */
    bloc next = { 77, FREE, 100} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(20) ;
    MU_ASSERT_EQUAL(ptr, heap+45+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c2_alloc_keep_selected_chunk) {
    bloc prev = {30, BUSY, 45} ;
    bloc current = {45, BUSY, 100} ;
    bloc next = {100, BUSY, 150} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(40) ;
    MU_ASSERT_EQUAL(ptr, heap+45+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c2_alloc_exact_fit_selected_chunk) {
    bloc prev = {30, BUSY, 45} ;
    bloc current = {45, BUSY, 100} ;
    bloc next = {100, BUSY, 150} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(55-3*SIZEOF_UINT) ;
    MU_ASSERT_EQUAL(ptr, heap+45+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c5_alloc_split_selected_chunk) {
    bloc prev = {150, BUSY, 200} ;
    bloc current = {200, BUSY, 512} ;
    bloc next = {512, FREE, 1000} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(300) ;
    MU_ASSERT_EQUAL(ptr, heap+200+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c7_alloc_split_selected_chunk) {
    bloc prev = {1000, BUSY, 1100} ;
    bloc current = {1100, BUSY, 1912} ;
    bloc next = {1912, FREE, HEAPSIZE} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(800) ;
    MU_ASSERT_EQUAL(ptr, heap+1100+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_malloc, c7_alloc_keep_selected_chunk) {
    bloc prev = {1000, BUSY, 1100} ;
    bloc current = {1100, BUSY, HEAPSIZE} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(HEAPSIZE-1100-18) ;
    MU_ASSERT_EQUAL(ptr, heap+1100+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
}
MU_TEST(my_malloc, c7_alloc_exact_fit_selected_chunk) {
    bloc prev = {1000, BUSY, 1100} ;
    bloc current = {1100, BUSY, HEAPSIZE} ;
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(HEAPSIZE-1100-3*SIZEOF_UINT) ;
    MU_ASSERT_EQUAL(ptr, heap+1100+2*SIZEOF_UINT) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
}
MU_TEST(my_malloc, out_of_memory) {
    unsigned char *ptr ;

    ptr = (unsigned char *) my_malloc(HEAPSIZE-1100-3*SIZEOF_UINT+1) ;
    MU_ASSERT_EQUAL(ptr, NULL) ;
}
MU_TEST_SUITE(my_malloc) = {
    MU_ADD_TEST(my_malloc, c0_alloc_split_selected_chunk),
    MU_ADD_TEST(my_malloc, c0_alloc_keep_selected_chunk),
    MU_ADD_TEST(my_malloc, c0_alloc_exact_fit),
    MU_ADD_TEST(my_malloc, c2_alloc_split_selected_chunk),
    MU_ADD_TEST(my_malloc, c2_alloc_keep_selected_chunk),
    MU_ADD_TEST(my_malloc, c2_alloc_exact_fit_selected_chunk),
    MU_ADD_TEST(my_malloc, c5_alloc_split_selected_chunk),
    MU_ADD_TEST(my_malloc, c7_alloc_keep_selected_chunk),
    MU_ADD_TEST(my_malloc, c7_alloc_exact_fit_selected_chunk),
    MU_ADD_TEST(my_malloc, out_of_memory),
    MU_TEST_SUITE_END
} ;

/****************** my_free *******************/

static bloc my_free_init[] = {  {   0, BUSY,   30},    /* c0:  30 octets libres   */
                                {  30, BUSY,   45},    /* c1:  10 octets réservés */
                                {  45, FREE,  100},    /* c2:  55 octets libres   */
                                { 100, BUSY,  150},    /* c3:  50 octets réservés */
                                { 150, BUSY,  200},    /* c4:  50 octets réservés */
                                { 200, FREE, 1000},    /* c5: 800 octets libres   */
                                {1000, BUSY, 1100},    /* c6: 100 octets réservés */
                                {1100, FREE, HEAPSIZE}} ;  /* c7: 948 octets libres   */

MU_SETUP(my_free) {
    int i ;
    for (i = 0 ; i < HEAPSIZE ; i++) {
        heap[i] = MAGIC ;
    }
    for (i= 0 ; i < sizeof(my_malloc_init)/sizeof(bloc); i++) {
        mu_set_bloc(my_free_init[i]) ;
    }
}
MU_TEAR_DOWN(my_free) {
}
MU_TEST(my_free, free_c0) {
    bloc current = {0, FREE, 30} ;
    bloc next = {30, BUSY, 45} ;

    my_free(heap +  0 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_free, free_c0_and_following) {
    /* Alter initial conditions */
    bloc new_c0 = { 0, BUSY, 45} ; /* Overlays C0 and c1 */

    bloc current = {0, FREE, 100} ;
    bloc next = {100, BUSY, 150} ;

    mu_set_bloc(new_c0) ;
    my_free(heap +  0 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_free, free_c1) {
    bloc prev = { 0, BUSY, 30} ;
    bloc current = {30, FREE, 100} ;
    bloc next = {100, BUSY, 150} ;

    my_free(heap +  30 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_free, free_c3) {
    bloc prev = { 30, BUSY, 45} ;
    bloc current = {45, FREE, 150} ;
    bloc next = {150, BUSY, 200} ;

    my_free(heap +  100 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_free, free_c4) {
    bloc prev = { 100, BUSY, 150} ;
    bloc current = {150, FREE, 1000} ;
    bloc next = {1000, BUSY, 1100} ;

    my_free(heap +  150 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
    CHECK_BLOC(next) ;
}
MU_TEST(my_free, free_c6) {
    bloc prev = { 150, BUSY, 200} ;
    bloc current = {200, FREE, HEAPSIZE} ;

    my_free(heap +  1000 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
}
MU_TEST(my_free, free_c7) {
    /* Alter initial conditions */
    bloc new_c7 = { 1100, BUSY, HEAPSIZE} ; /* Overlays C0 and c1 */

    bloc prev = { 1000, BUSY, 1100} ;
    bloc current = {1100, FREE, HEAPSIZE} ;

    mu_set_bloc(new_c7) ;
    my_free(heap +  1100 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
}
MU_TEST(my_free, free_c7_and_previous) {
    /* Alter initial conditions */
    bloc new_c6 = {200, FREE, 1100} ;
    bloc new_c7 = { 1100, BUSY, HEAPSIZE} ; /* Overlays C0 and c1 */

    bloc prev = { 150, BUSY, 200} ;
    bloc current = {200, FREE, HEAPSIZE} ;

    mu_set_bloc(new_c6) ;
    mu_set_bloc(new_c7) ;
    my_free(heap +  1100 + 2*sizeof(unsigned int)) ;

    CHECK_BLOC(prev) ;
    CHECK_BLOC(current) ;
}
MU_TEST_SUITE(my_free) = {
    MU_ADD_TEST(my_free, free_c0_and_following),
    MU_ADD_TEST(my_free, free_c0),
    MU_ADD_TEST(my_free, free_c1),
    MU_ADD_TEST(my_free, free_c3),
    MU_ADD_TEST(my_free, free_c4),
    MU_ADD_TEST(my_free, free_c6),
    MU_ADD_TEST(my_free, free_c7),
    MU_ADD_TEST(my_free, free_c7_and_previous),
    MU_TEST_SUITE_END
} ;

/****************** main *******************/

int main(int argc, char **argv) {
    MU_RUN_TEST_SUITE_WITH_REPORT(get_int) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(set_int) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(set_chunk) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(get_chunk) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(init_alloc) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(my_malloc) ;
    MU_RUN_TEST_SUITE_WITH_REPORT(my_free) ;
    return 0 ;
}

