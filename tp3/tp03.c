#include "tp03.h"

unsigned int get_int(void *ptr) {
	/* Retourne la valeur du pointeur préalablement casté */
	return *(unsigned int *) ptr;
}

void set_int(void *ptr, unsigned int val) {
	/* Caste le pointeur puis lui assigne la valeur donnée */
	*(unsigned int *)ptr = val;
}

void set_chunk(chunk *c, unsigned char *ptr) {
	/*  On assigne au pointeur l'état du chunk (libre ou occupé) */
	set_int(ptr, c->free); /* Équivaut à (*c).free */
	/* sizeof(unsigned int) renvoie 4,
	donc à l'adresse du pointeur + 4, on assigne la taille du chunk */
	set_int(ptr + sizeof(unsigned int), c->size);
	/* Comme on le voit dans l'énoncé, à la fin du bloc, on redonne la taille du chunk pour une lecture plus facile.
	Donc à l'adresse du pointeur + la taille du chunk - 4 (on avait assigné un entier pour l'état), on assigne la taille du chunk */
	set_int(ptr + c->size - sizeof(unsigned int), c->size);
}

void get_chunk(chunk *c, unsigned char *ptr) {
	c->free = get_int(ptr);
	c->size = get_int(ptr + sizeof(unsigned int)); /* On récupère la taille du chunk dans la case suivante de HEAP */
	c->addr = ptr;

	if ( ptr == heap ) { /* Si le pointeur est le début de HEAP et donc qu'il n'y a pas de chunk avant */
		c->previous_chunk = NULL;
		c->next_chunk = ptr + get_int(ptr + sizeof(unsigned int));
	} else if ( ptr + c->size == &heap[HEAPSIZE] ) { /* Si le pointeur est la fin de HEAP et donc qu'il n'y a pas de chunk avant */
		c->previous_chunk = ptr - get_int(ptr - sizeof(unsigned int));
		c->next_chunk = NULL;
	} else { /* Pointeur standard */
		c->previous_chunk = ptr - get_int(ptr - sizeof(unsigned int));
		c->next_chunk = ptr + get_int(ptr + sizeof(unsigned int));
	}
}

void init_alloc() {
	chunk initial;
	initial.free = FREE;
	initial.size = HEAPSIZE;
	set_chunk(&initial, heap);
}

/* Voir cours/cours.txt */
#define CHUNK_PAS_COMPATIBLE(chunk, size) ( (chunk).free == BUSY || (chunk).size < ( (size) + 3 * sizeof(unsigned int) ) ) 
#define CHUNK_TROUVE_TROP_PETIT(size) (size >= 0 && size <= 3 * sizeof(unsigned int))

void *my_malloc(unsigned int size) {	
	/* On déclare les chunks */
	int sizeTmp = 0;
	chunk new_one, new_two;
	get_chunk(&new_one, heap);

	/* Test de départ sur la valeur de size */
	if (size <= 0) return NULL;

	/* Parcours du tableau heap jusqu'à trouver le chunk adéquat */
	/* On déroule le heap */
	while (CHUNK_PAS_COMPATIBLE(new_one, size)) {
		/* Le chunk ne convient pas, On avance jusqu'au prochain chunk */
		if (new_one.next_chunk == NULL) return NULL; /* C'est le dernier, il n'est pas compatible-> on sort) */
			
		get_chunk(&new_one, new_one.addr + new_one.size);
	}
	
	/* Le chunk est compatible */
	sizeTmp = new_one.size - (size + 3 * sizeof(unsigned int));
	
	if (CHUNK_TROUVE_TROP_PETIT(sizeTmp)) {
		/* On ne définit que le premier chunk, sans toucher à la taille */
		new_one.free = BUSY;
		set_chunk(&new_one, new_one.addr);
	} else {
		/* Premier chunk */
		new_one.size = size + 3 * sizeof(unsigned int);
		new_one.free = BUSY;
		set_chunk(&new_one, new_one.addr);

		/* Second Chunk */
		get_chunk(&new_two, new_one.addr + new_one.size);
		new_two.size = sizeTmp;
		new_two.free = FREE;
		set_chunk(&new_two, new_one.addr + new_one.size);
	}
	
	return new_one.addr + 2 * sizeof(unsigned int);
}

/* Voir cours/cours.txt */
#define CHUNK_N_EST_PAS_NUL(chunk) ( (chunk) != NULL )
#define EST_LIBRE(chunk) ( (chunk).free == FREE )

void my_free(void *ptr) {
	chunk current, previous, next;
	int size = 0;

	/* On caste ptrU pour pouvoir faire des calculs arithémtiques dessus */
	unsigned char *ptrU = (unsigned char*)ptr;
	
	/* On réduit ptr de 2 entiers non signés qui correspond à la taille des 2 cases (taille et free) */
	ptrU -= 2*sizeof(unsigned int);
	
	/* On assigne au chunk la valeur du chunk où pointe ptrU et on initialise size */
	get_chunk(&current, ptrU);
	size = current.size;

	/* On vérifie avant tout test que les chunks alentours ne sont pas nuls */
	/* On fait les tests sur les chunk alentours */
	
	if ( CHUNK_N_EST_PAS_NUL(current.next_chunk) ) {
		get_chunk(&next, current.next_chunk);

		if ( EST_LIBRE(next) ) {
			size += next.size;
		}
	}
	
	if ( CHUNK_N_EST_PAS_NUL(current.previous_chunk) ) {
		get_chunk(&previous, current.previous_chunk);

		if ( EST_LIBRE(previous) ) {
			size += previous.size;

			/* On change l'adresse de current qui recule d'un chunk */
			current.addr = current.previous_chunk;
		}
	}

	/* Redéfinition des attributs de current */
	current.free = FREE;
	current.size = size;

	/* On applique tout ça */
	set_chunk(&current, current.addr);
}

void *my_realloc(void *ptr, unsigned int size) {
	chunk current;

	/* On récupère les informations du chunk pointé */
	get_chunk(&current, (unsigned char *) ptr);

	/* On vérifie que la taille est bien supérieur à celle du chunk pointé par ptr */
	if (size <= current.size) return ptr;

	/* On libère l'espace occupé par le pointeur précédent */
	my_free(ptr);
	
	/* On initialise la taille du chunk */
	current.size = size;

	/* On lui assigne l'adresse d'un nouveau chunk de taille adéquat */
	current.addr = my_malloc(size);

	/* On applique le tout */
	set_chunk(&current, current.addr);

	return current.addr;
}