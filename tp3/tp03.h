#include <stdio.h>


/* DEFINES */
#define HEAPSIZE 2048
#define FREE 1 /* Définit l'état d'un chunk */
#define BUSY 0

/* STRUCTURES */
typedef struct
{
	unsigned int free;
	unsigned int size;
	unsigned char *addr;
	unsigned char *next_chunk;
	unsigned char *previous_chunk;
} chunk;

/* VARIABLES */
extern unsigned char heap[HEAPSIZE];

/* DÉCLARATIONS EXPLICITES */
unsigned int get_int(void *ptr);
void set_int(void *ptr, unsigned int val);
void set_chunk(chunk *c, unsigned char *ptr);
void get_chunk(chunk *c, unsigned char *ptr);
void init_alloc();
void *my_malloc(unsigned int size);
void my_free(void *ptr);
void *my_realloc(void *ptr, unsigned int size);