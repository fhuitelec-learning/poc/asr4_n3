#include "tp03.h"

unsigned char heap[HEAPSIZE];

void questionUn() {
	int i;
	void *p;
	heap[0] = 255; /* 245 = 00 00 00 f5 */
	p = &heap[0];
	
	printf("\nQuestion 1 : \n");
	printf("Fonction : get_int\n");
	printf("Valeur : %d\nFor : ", get_int(p));

	/* Un entier non signé tient sur 4 octets,
	pendant 4, octets, la valeur en hexa de 245 est affichée */
	for (i = 0 ; i < sizeof(unsigned int) ; i++) { /* sizeof(unsigned int) est égal à 4 octets */
		printf("%2X ", heap[i]); /* Cette notation (%2X) affiche la valeur d'un octet en hexa */
	}
	printf("\n");

	set_int(&heap[10], 999) ; /* 999 = 00 00 03 E7 */
	printf("\nFonction : set_int\n");
	printf("Valeur : %d\nFor : ", heap[10]);

	/* Un entier non signé tient sur 4 octets,
	pendant 4, octets, la valeur en hexa de 245 est affichée */
	for (i = 0; i < sizeof(unsigned int); i++) { /* sizeof(unsigned int) est égal à 4 octets */
		printf("%2X ", heap[10 + i]); /* Cette notation (%2X) affiche la valeur d'un octet en hexa */
	}
	printf("\n");
}

void questionDeux() {
	chunk un;
	un.free = BUSY; /* On déclare ce chunk occupé - Voir BUSY dans tp03.h */
	un.size = 25; /* On lui alloue sa taille */

	printf("\nQuestion 2 : \n");
	set_chunk(&un, &heap[20]);
	printf("État du chunk (heap[20]) : %d\nTaille (heap[20 + 25]) : %d\nTaille fin (heap[20 - 4 + 25]) : %d\n", heap[20], heap[20 + 4], heap[20+25-4]);
}

void questionTrois() {
	chunk un, deux;
	deux.free = BUSY;
	deux.size = 12;
	
	printf("\nQuestion 3 : \n");
	set_chunk(&deux, &heap[0]);
	get_chunk(&un, heap);
	printf("Free : %d \nSize : %d \nAddr : %p \nAddr heap[0] : %p\nPrevious : %p\nNext : %p\n", un.free, un.size, un.addr, heap, un.previous_chunk, un.next_chunk);
}

void questionQuatre() {
	printf("\nQuestion 4 :");
	init_alloc();
	printf("\nInitialisé !\n");
}

int main(int argc, char *argv[]) {
	questionUn();
	questionDeux();
	questionTrois();
	questionQuatre();
	questionCinq();
	return 0;
}