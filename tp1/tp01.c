#include <stdio.h>
#include "rat.h"
/* Standard input/output */

void afficher(int liste[], int taille) {
	int i, zero = 0;

	for (i = 0; i < taille && !zero; i++) {
		printf("%d ",liste[i]);
		if (liste[i + 1] == 0) {
			zero = 1;
		}
	}
	printf("\n");
}

int somme(int liste[], int taille) {
	int i, somme = 0;
	for (i = 0; i < taille;
	 i++) {
		somme = somme + i;
	}
	return somme;
}

int *copie_dans(int dest[], int src[], int taille) {
	int i;
	for (i = 0; i < taille; ++i) {
		dest[i] = src[i];
	}
	return dest;
}

int *ajoute_apres(int liste_dest[], int taille_dest, int liste_src[], int taille_src) {
	int i, j = 0;

	for (i = taille_dest; i < taille_src + taille_dest; i++, j++) {
		liste_dest[i] = liste_src[j];
	}
	return liste_dest;
}

struct rat rat_produit(struct rat n1, struct rat n2) {
	struct rat n3;
	n3.num = n1.num * n2.num;
	n3.den = n1.den * n2.den;

	return n3;
}

struct rat rat_somme(struct rat n1, struct rat n2) {
	struct rat n3;
	n3.den = n1.den * n2.den;
	n3.num = n1.num * n2.den + n2.num * n1.den;
	
	return n3;
}

struct rat rat_plus_petit(struct rat liste_rat[]) {
	int i, plus_petit = 0, fini = 0;
	float tmp = (float)liste_rat[0].num/liste_rat[0].den;

	for (i = 1; fini != 1; i++) {
		if(tmp > liste_rat[i].num/liste_rat[i].den) {
			plus_petit = i;
			tmp = liste_rat[i].num/liste_rat[i].den;
		}
 
		if (liste_rat[i+1].den == 0) {
			fini = 1;
		}
	}

	return liste_rat[plus_petit];
}

void afficher_rat(struct rat liste_rat[]) {
	int i, fini = 0;

	printf("%d/%d", liste_rat[0].num, liste_rat[0].den);

	for (i = 1; fini != 1; i++) {
		printf(", %d/%d", liste_rat[i].num, liste_rat[i].den);

		if (liste_rat[i+1].den == 0) {
			fini = 1;
		}
	}
}