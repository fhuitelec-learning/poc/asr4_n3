#include <stdio.h>
#include "rat.h"

void afficher(int liste[], int taille);
int somme(int liste[], int taille);
int *copie_dans(int dest[], int src[], int taille);
int *ajoute_apres(int liste_dest[], int taille_dest, int liste_src[], int taille_src);
void afficher_rat(struct rat liste_rat[]);

struct rat rat_produit(struct rat n1, struct rat n2);
struct rat rat_somme(struct rat n1, struct rat n2);
struct rat rat_plus_petit(struct rat liste_rat[]);