#include <stdio.h>
#include "tp01.h"

#define TAILLE 10
#define TAILLE_SRC 3
#define TAILLE_DEST 20
#define NB_ELEMENTS_DEST 3
#define TAILLE_RAT 11
/* TAILLE est une variable globale constante */

void questionUn(int liste[]) {
	/* On affiche la liste */
	color("\nEX1 - Question 1\n", "VERT");
	printf("Affichage :\n");
	afficher(liste,TAILLE);
}

void questionDeux(int liste[]) {
	/* On fait la somme de la première liste qui est ensuite affichée */
	color("\nEX1 - Question 2\n", "VERT");
	printf("Somme : %d\n", somme(liste, TAILLE));
}

void questionTrois(int src[], int dest[]) {
	color("\nEX1 - Question 3\n", "VERT");
	/* On affiche la liste destination initiale */
	printf("Liste Dest initiale : ");
	afficher(dest, TAILLE);
	printf("Liste SRC : ");
	afficher(src, TAILLE);
	/* On fait la copie de la liste src dans la liste dest */
	copie_dans(dest, src, TAILLE);
	/* On affiche ensuite la nouvelle liste dest qui contient la liste src */
	printf("Liste DEST réécrite : ");
	afficher(dest, TAILLE);
}

void questionQuatre(int liste_dest[], int liste_src[]) {
	color("\nEX1 - Question 4\n", "VERT");
	printf("Première liste : ");
	afficher(liste_dest ,TAILLE_DEST);
	printf("Deuxième liste : ");
	afficher(liste_src,TAILLE_SRC);
	ajoute_apres(liste_dest, NB_ELEMENTS_DEST, liste_src, TAILLE_SRC);
	printf("Liste concaténée : ");
	afficher(liste_dest, TAILLE_DEST);
}

void questionCinq(struct rat n1, struct rat n2) {
	struct rat n3 = rat_produit(n1,n2);

	color("\nEX2 - Question 1\n", "VERT");
	printf("%d/%d et %d/%d\n", n1.num, n1.den, n2.num, n2.den);
	printf("Résultat :\n%d/%d\n", n3.num, n3.den);
}

void questionSix(struct rat n1, struct rat n2) {
	struct rat n3 = rat_somme(n1,n2);

	color("\nEX2 - Question 2\n", "VERT");
	printf("%d/%d et %d/%d\n", n1.num, n1.den, n2.num, n2.den);
	printf("Résultat :\n%d/%d\n", n3.num, n3.den);
}

void questionSept(struct rat liste_Rat[]) {
	struct rat n = rat_plus_petit(liste_Rat);
	color("\nEX2 - Question 3\n", "VERT");
	afficher_rat(liste_Rat);
	printf("\nRésultat :\n%d/%d\n\n", n.num, n.den);
}

int main(void) {
	/* Listes pour les questions 1, 2, 3 */
	int liste[TAILLE] = {0, 1, 2}, dest[TAILLE] = {0, 1, 2, 3, 5, 4, 6, 8, 6, 54}, src[TAILLE]= {3, 4, 5, 0, 0, 0, 6, 8, 6, 54};
	/* Listes pour la question 4 */
	int liste_dest[TAILLE_DEST] = {0, 1, 2}, liste_src[TAILLE_SRC] = {3, 4, 5};
	/* Nombres Rationnels pour l'exercice 2 */
	struct rat n1 = {1,2}, n2 = {78,4}, n3 = {45,8}, n4 = {5,12};
	/*,n5 = {13,5}, n6 = {56,8};*/
	struct rat liste_rat[TAILLE_RAT] = {
	{0,0},
	{8,9},
	{1,7},
	{1,1},
	{7,9},
	{5,8},
	{8,2}, /* smallest  one */
	{5,6},
	{7,5},
	{9,8},
	{0,0}
    };

	/* EX 1 */
	questionUn(liste);
	questionDeux(liste);
	questionTrois(src, dest);
	questionQuatre(liste_dest, liste_src);
	
	/* EX 2 */
	questionCinq(n1, n2);
	questionSix(n3, n4);
	questionSept(liste_rat);

	return 0;
}